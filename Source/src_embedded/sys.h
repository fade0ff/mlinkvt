/** System configuration
	Configuration of system resources (CPU clock, prescalers etc.)
	LPC82x ARM Cortex M0+
	----------------------------------------------------------
	Copyright 2015 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#ifndef SYS_H
#define SYS_H

#include "sys_cnf.h"

/** Internal oscillator frequency */
#define SYS_FRQ_IRC    12000000UL

/* System memory remap register SYSMEMREMAP */
#define SYSCON_SYSMEM_BOOT_LOADER  0   ///! Bootloader Mode. Interrupt vectors are re-mapped to Boot ROM.
#define SYSCON_SYSMEM_USER_RAM     1   ///! User RAM Mode. Interrupt vectors are re-mapped to Static RAM.
#define SYSCON_SYSMEM_USER_FLASH   2   ///! User Flash Mode. Interrupt vectors are not re-mapped and reside in Flash.

/** Set System memory remap register */
#define SYSCON_SetSysmemRemap(x)   { LPC_SYSCON->SYSMEMREMAP  = (x); }

/* Peripheral reset control register PRESETCTRL */
#define SYSCON_SetPeripheralReset(x)   { LPC_SYSCON->PRESETCTRL  = (x); }


/* PLL and clock control */
#define SYSCON_OSCCTL_BYPASS_OSC      (1UL<<0) ///! Bypass system oscillator (to use external clock)
#define SYSCON_OSCCTL_FREQRANGE_20MHZ 0        ///! Oscillator range 1-20MHz
#define SYSCON_OSCCTL_FREQRANGE_25MHZ (1UL<<1) ///! Oscillator range 15-25MHz

#define SYSCON_SetOscCtrl(x)   {LPC_SYSCON->SYSOSCCTRL  = (x);}

#define SYSCON_PLLSRC_IRC         (1UL<<0)  ///! internal RC oscillator
#define SYSCON_PLLSRC_XTAL        (1UL<<1)  ///! crystal oscillator
#define SYSCON_PLLSRC_CLKIN       (1UL<<1)  ///! external clock input

#define SYSCON_SetPLLSource(src)    {LPC_SYSCON->SYSPLLCLKSEL  = (src); LPC_SYSCON->SYSPLLCLKUEN  = 0; LPC_SYSCON->SYSPLLCLKUEN  = 1; }
#define SYSCON_PLL_Setup(msel, psel) {LPC_SYSCON->SYSPLLCTRL = ((msel) & 0x3F) | (((psel) & 0x3) << 6);}
#define SYSCON_PLL_Locked()          ((LPC_SYSCON->SYSPLLSTAT & 1) != 0)

#define SYSCON_CLKSRC_IRC       0			///! Internal RC oscillator
#define SYSCON_CLKSRC_PLL_IN    1			///! PLL Input
#define SYSCON_CLKSRC_WDOSC     2			///! Watchdog oscillator
#define SYSCON_CLKSRC_PLL_OUT   3			///! PLL Output

#define SYSCON_SelectSystemClock(x)  {LPC_SYSCON->MAINCLKSEL = (x); LPC_SYSCON->MAINCLKUEN=0; LPC_SYSCON->MAINCLKUEN=1; }

#define SYSCON_SetSysClockDivider(x) {LPC_SYSCON->SYSAHBCLKDIV = (x); }

#define SYSCON_CLKOUTSRC_IRC       0			///! Internal RC oscillator
#define SYSCON_CLKOUTSRC_XTAL      1			///! System oscillator
#define SYSCON_CLKOUTSRC_WDOSC     2			///! Watchdog oscillator
#define SYSCON_CLKOUTSRC_MAIN      3			///! Main clock


/* Watchdog */
#define SYSCON_WDT_FRQSEL_600KHZ   ( 1UL<<5) ///! analog wdt frequency is 600kHz
#define SYSCON_WDT_FRQSEL_1050KHZ  ( 2UL<<5) ///! analog wdt frequency is 1.05MHz
#define SYSCON_WDT_FRQSEL_1400KHZ  ( 3UL<<5) ///! analog wdt frequency is 1.4MHz
#define SYSCON_WDT_FRQSEL_1750KHZ  ( 4UL<<5) ///! analog wdt frequency is 1.75MHz
#define SYSCON_WDT_FRQSEL_2100KHZ  ( 5UL<<5) ///! analog wdt frequency is 2.1MHz
#define SYSCON_WDT_FRQSEL_2400KHZ  ( 6UL<<5) ///! analog wdt frequency is 2.4MHz
#define SYSCON_WDT_FRQSEL_2700KHZ  ( 7UL<<5) ///! analog wdt frequency is 2.7MHz
#define SYSCON_WDT_FRQSEL_3000KHZ  ( 8UL<<5) ///! analog wdt frequency is 3.0MHz
#define SYSCON_WDT_FRQSEL_3250KHZ  ( 9UL<<5) ///! analog wdt frequency is 3.25MHz
#define SYSCON_WDT_FRQSEL_3500KHZ  (10UL<<5) ///! analog wdt frequency is 3.5MHz
#define SYSCON_WDT_FRQSEL_3750KHZ  (11UL<<5) ///! analog wdt frequency is 3.75MHz
#define SYSCON_WDT_FRQSEL_4000KHZ  (12UL<<5) ///! analog wdt frequency is 4.0MHz
#define SYSCON_WDT_FRQSEL_4200KHZ  (13UL<<5) ///! analog wdt frequency is 4.2MHz
#define SYSCON_WDT_FRQSEL_4400KHZ  (14UL<<5) ///! analog wdt frequency is 4.4MHz
#define SYSCON_WDT_FRQSEL_4600KHZ  (15UL<<5) ///! analog wdt frequency is 4.6MHz

/** Set watchdog oscillator control register WDTOSCCTRL
    @param frqsel use  SYSCON_WDT_FRQSEL_x macros to select analog oscillator frequency
    @param divsel divider
*/
#define SYSCON_SetWdtOscControl(frqsel, divsel) { LPC_SYSCON->WDTOSCCTRL = (frqsel)|(((divsel)/2-1)&0xf); }


/* System reset status register SYSRSTSTAT */
#define SYSCON_RSTSTAT_POR     (1UL<<0)  ///! Power on reset detected
#define SYSCON_RSTSTAT_EXTRST  (2UL<<0)  ///! External reset detected
#define SYSCON_RSTSTAT_WDT     (3UL<<0)  ///! WDT reset detected
#define SYSCON_RSTSTAT_BOD     (4UL<<0)  ///! Brown out detection reset detected
#define SYSCON_RSTSTAT_SYSRST  (4UL<<0)  ///! System reset detected

#define SYSCON_GetSystemResetStatus() (LPC_SYSCON.SYSRSTSTAT)
#define SYSCON_ResetSystemResetStatus(x) {LPC_SYSCON.SYSRSTSTAT = (x);}

/* System clock register */
#define SYS_CLK_SYS        (1UL<<0)
#define SYS_CLK_ROM        (1UL<<1)
#define SYS_CLK_RAM0_1     (1UL<<2)
#define SYS_CLK_FLASHREG   (1UL<<3)
#define SYS_CLK_FLASH      (1UL<<4)
#define SYS_CLK_I2C0       (1UL<<5)
#define SYS_CLK_GPIO       (1UL<<6)
#define SYS_CLK_SWM        (1UL<<7)
#define SYS_CLK_SCT        (1UL<<8)
#define SYS_CLK_WKT        (1UL<<9)
#define SYS_CLK_MRT        (1UL<<10)
#define SYS_CLK_SPI0       (1UL<<11)
#define SYS_CLK_SPI1       (1UL<<12)
#define SYS_CLK_CRC        (1UL<<13)
#define SYS_CLK_UART0      (1UL<<14)
#define SYS_CLK_UART1      (1UL<<15)
#define SYS_CLK_UART2      (1UL<<16)
#define SYS_CLK_WWDT       (1UL<<17)
#define SYS_CLK_IOCON      (1UL<<18)
#define SYS_CLK_ACMP       (1UL<<19)
#define SYS_CLK_I2C1       (1UL<<21)
#define SYS_CLK_I2C2       (1UL<<22)
#define SYS_CLK_I2C3       (1UL<<23)
#define SYS_CLK_ADC        (1UL<<24)
#define SYS_CLK_MTB        (1UL<<26)
#define SYS_CLK_DMA        (1UL<<29)

/*  System clock control register */

#define SYSCON_SetClockControl(x) { LPC_SYSCON->SYSAHBCLKCTRL = (x); }

#define SYSCON_EnableClockControl(x) { LPC_SYSCON->SYSAHBCLKCTRL |= (x); }
#define SYSCON_DisableClockControl(x) { LPC_SYSCON->SYSAHBCLKCTRL &= ~(u32)(x); }

/* System clock register */
#define SYS_CLK_SYS_CFG(x)      (((u32)(x)&1)<<0)
#define SYS_CLK_ROM_CFG(x)      (((u32)(x)&1)<<1)
#define SYS_CLK_RAM0_1_CFG(x)   (((u32)(x)&1)<<2)
#define SYS_CLK_FLASHREG_CFG(x) (((u32)(x)&1)<<3)
#define SYS_CLK_FLASH_CFG(x)    (((u32)(x)&1)<<4)
#define SYS_CLK_I2C0_CFG(x)     (((u32)(x)&1)<<5)
#define SYS_CLK_GPIO_CFG(x)     (((u32)(x)&1)<<6)
#define SYS_CLK_SWM_CFG(x)      (((u32)(x)&1)<<7)
#define SYS_CLK_SCT_CFG(x)      (((u32)(x)&1)<<8)
#define SYS_CLK_WKT_CFG(x)      (((u32)(x)&1)<<9)
#define SYS_CLK_MRT_CFG(x)      (((u32)(x)&1)<<10)
#define SYS_CLK_SPI0_CFG(x)     (((u32)(x)&1)<<11)
#define SYS_CLK_SPI1_CFG(x)     (((u32)(x)&1)<<12)
#define SYS_CLK_CRC_CFG(x)      (((u32)(x)&1)<<13)
#define SYS_CLK_UART0_CFG(x)    (((u32)(x)&1)<<14)
#define SYS_CLK_UART1_CFG(x)    (((u32)(x)&1)<<15)
#define SYS_CLK_UART2_CFG(x)    (((u32)(x)&1)<<16)
#define SYS_CLK_WWDT_CFG(x)     (((u32)(x)&1)<<17)
#define SYS_CLK_IOCON_CFG(x)    (((u32)(x)&1)<<18)
#define SYS_CLK_ACMP_CFG(x)     (((u32)(x)&1)<<19)
#define SYS_CLK_I2C1_CFG(x)     (((u32)(x)&1)<<21)
#define SYS_CLK_I2C2_CFG(x)     (((u32)(x)&1)<<22)
#define SYS_CLK_I2C3_CFG(x)     (((u32)(x)&1)<<23)
#define SYS_CLK_ADC_CFG(x)      (((u32)(x)&1)<<24)
#define SYS_CLK_MTB_CFG(x)      (((u32)(x)&1)<<26)
#define SYS_CLK_DMA_CFG(x)      (((u32)(x)&1)<<29)


/* Power configuration register */
#define SYSCON_POWERDOWN_IRCOUT_PD  (1UL << 0)	///! IRC oscillator output power-down  */
#define SYSCON_POWERDOWN_IRC        (1UL << 1)	///! IRC oscillator power-down*/
#define SYSCON_POWERDOWN_FLASH      (1UL << 2)	///! Flash memory power-down*/
#define SYSCON_POWERDOWN_BOD_PD     (1UL << 3)	///! BOD power-down*/
#define SYSCON_POWERDOWN_ADC_PD     (1UL << 4)	///! ADC0 power-down */
#define SYSCON_POWERDOWN_SYSOSC_PD  (1UL << 5)	///! System oscillator power-down */
#define SYSCON_POWERDOWN_WDTOSC_PD  (1UL << 6)	///! Watchdog oscillator power-down */
#define SYSCON_POWERDOWN_SYSPLL_PD  (1UL << 7)	///! System PLL power-down */
#define SYSCON_POWERDOWN_ACMP_PD    (1UL << 15)	///! ACMP0 power-down */

#define SYSCON_PowerUp(x)               {LPC_SYSCON->PDRUNCFG &= (uint32_t)~(x);}
#define SYSCON_PowerDown(x)             {LPC_SYSCON->PDRUNCFG |= (uint32_t)(x);}


/* Check XTAL */
#if SYS_FRQ_OSC<1000000 || SYS_FRQ_OSC > 25000000
#error "SYS_FRQ_OSC setup out of range"
#endif

#if SYS_FRQ_WDT_DIV > 64 || (SYS_FRQ_WDT_DIV%2)!=0
#error "SYS_FRQ_WDT_DIV must be even and <=64"
#endif

/* PLL setup */
#if SYS_MSEL_CFG<1 || SYS_MSEL_CFG>64
#error "MSEL value for PLL setup out of range"
#endif

#if !(SYS_PSEL_CFG==1 || SYS_PSEL_CFG==2 || SYS_PSEL_CFG==4 || SYS_PSEL1_CFG==8)
#error "PSEL value for SYS PLL setup out of range"
#endif

#if  (SYS_FRQ_WDT_FRQSEL ==  0)
#error "SYS_FRQ_WDT_FRQSEL undefined!"
#elif (SYS_FRQ_WDT_FRQSEL ==  SYSCON_WDT_FRQSEL_600KHZ)
#define __WDT_OSC_CLK          ( 600000 / SYS_FRQ_WDT_DIV)
#elif (SYS_FRQ_WDT_FRQSEL ==  SYSCON_WDT_FRQSEL_1050KHZ)
#define __WDT_OSC_CLK          (1050000 / SYS_FRQ_WDT_DIV)
#elif (SYS_FRQ_WDT_FRQSEL ==  SYSCON_WDT_FRQSEL_1400KHZ)
#define __WDT_OSC_CLK          (1400000 / SYS_FRQ_WDT_DIV)
#elif (SYS_FRQ_WDT_FRQSEL ==  SYSCON_WDT_FRQSEL_1750KHZ)
#define __WDT_OSC_CLK          (1750000 / SYS_FRQ_WDT_DIV)
#elif (SYS_FRQ_WDT_FRQSEL ==  SYSCON_WDT_FRQSEL_2100KHZ)
#define __WDT_OSC_CLK          (2100000 / SYS_FRQ_WDT_DIV)
#elif (SYS_FRQ_WDT_FRQSEL ==  SYSCON_WDT_FRQSEL_2400KHZ)
#define __WDT_OSC_CLK          (2400000 / SYS_FRQ_WDT_DIV)
#elif (SYS_FRQ_WDT_FRQSEL ==  SYSCON_WDT_FRQSEL_2700KHZ)
#define __WDT_OSC_CLK          (2700000 / SYS_FRQ_WDT_DIV)
#elif (SYS_FRQ_WDT_FRQSEL ==  SYSCON_WDT_FRQSEL_3000KHZ)
#define __WDT_OSC_CLK          (3000000 / SYS_FRQ_WDT_DIV)
#elif (SYS_FRQ_WDT_FRQSEL ==  SYSCON_WDT_FRQSEL_3250KHZ)
#define __WDT_OSC_CLK          (3250000 / SYS_FRQ_WDT_DIV)
#elif (SYS_FRQ_WDT_FRQSEL == SYSCON_WDT_FRQSEL_3500KHZ)
#define __WDT_OSC_CLK          (3500000 / SYS_FRQ_WDT_DIV)
#elif (SYS_FRQ_WDT_FRQSEL == SYSCON_WDT_FRQSEL_3750KHZ)
#define __WDT_OSC_CLK          (3750000 / SYS_FRQ_WDT_DIV)
#elif (SYS_FRQ_WDT_FRQSEL == SYSCON_WDT_FRQSEL_4000KHZ)
#define __WDT_OSC_CLK          (4000000 / SYS_FRQ_WDT_DIV)
#elif (SYS_FRQ_WDT_FRQSEL == SYSCON_WDT_FRQSEL_4200KHZ)
#define __WDT_OSC_CLK          (4200000 / SYS_FRQ_WDT_DIV)
#elif (SYS_FRQ_WDT_FRQSEL == SYSCON_WDT_FRQSEL_4400KHZ)
#define __WDT_OSC_CLK          (4400000 / SYS_FRQ_WDT_DIV)
#else
#define __WDT_OSC_CLK          (4600000 / SYS_FRQ_WDT_DIV)
#endif

#if __WDT_OSC_CLK != SYS_FRQ_WDT
#error "SYS_FRQ_WDT doesn't match the values set by SYS_FRQ_WDT_FRQSEL and SYS_FRQ_WDT_DIV"
#endif

#if SYS_PSEL_CFG==8
#define SYS_PSEL_CFG_ 3
#else
#define SYS_PSEL_CFG_ (SYS_PSEL_CFG/2)
#endif

#define SYSPLLCFG_CFG (((SYS_MSEL_CFG - 1)&0x3f) | (((SYS_PSEL_CFG_)&3)<<6))

/* check frequency setup */
#if SYS_PLL_CLKSRC_CFG  == SYSCON_PLLSRC_XTAL
#define SYS_FRQ_PLLIN SYS_FRQ_OSC
#else
#define SYS_FRQ_PLLIN SYS_FRQ_IRC
#endif

#define SYS_FRQ_PLL (SYS_FRQ_PLLIN*SYS_MSEL_CFG)

#if SYS_FRQ_PLL > 100000000UL
#error "Maximum system PLL output frequency exceeded"
#endif

#if (SYS_FRQ_PLL*2*SYS_PSEL_CFG < 156000000UL) || (SYS_FRQ_PLL*2*SYS_PSEL_CFG > 320000000UL)
#error "Limits of FCCO frequency exceeded"
#endif


/* Main clock */
#if SYS_MAIN_CLKSEL_CFG == SYSCON_CLKSRC_WDOSC
#define SYS_FRQ_MAIN SYSCON_WDTOSC_FREQ
#elif SYS_MAIN_CLKSEL_CFG == SYSCON_CLKSRC_IRC
#define SYS_FRQ_MAIN SYS_FRQ_IRC
#elif  SYS_MAIN_CLKSEL_CFG == SYSCON_CLKSRC_PLL_IN
#define SYS_FRQ_MAIN SYS_FRQ_PLLIN
#else
#define SYS_FRQ_MAIN SYS_FRQ_PLL
#endif

#if (SYS_FRQ_MAIN/SYS_CLK_DIV) != SYS_FRQ_CORE
#error "Clock setup doesn't fit expected value"
#endif

/* Clockout */
#if SYS_CLK_CLKOUT_CLKSEL_CFG == SYSCON_CLKOUTSRC_WDOSC
#define SYS_FRQ_CLKOUT_BASE SYSCTL_WDTOSC_FREQ
#elif SYS_CLK_CLKOUT_CLKSEL_CFG == SYSCON_CLKOUTSRC_IRC
#define SYS_FRQ_CLKOUT_BASE SYSCTL_IRC_FREQ
#elif  SYS_CLK_CLKOUT_CLKSEL_CFG == SYSCON_CLKOUTSRC_XTAL
#define SYS_FRQ_CLKOUT_BASE SYS_FRQ_OSC
#else
#define SYS_FRQ_CLKOUT_BASE SYS_FRQ_MAIN
#endif


#define SYS_FRQ               SYS_FRQ_CORE

#if SYS_FRQ % 1000 == 0
#define SYS_FRQ_US(us) ((SYS_FRQ_CORE/1000)*(us))/1000
#else
#define SYS_FRQ_US(us) (SYS_FRQ_CORE*(us))/1000000
#endif

/* Peripheral clocks */
#define SYS_CLK_SYSTICK_FRQ   (SYS_FRQ/SYS_CLK_SYSTICK_DIV)
#define SYS_CLK_UART_FRQ      (SYS_FRQ/SYS_CLK_UART_DIV)
#define SYS_CLK_IOCON_FRQ     (SYS_FRQ/SYS_CLK_IOCON_DIV)
#define SYS_CLK_TRACE_FRQ     (SYS_FRQ/SYS_CLK_TRACE_DIV)

#define SYS_CLK_SPI_FRQ       SYS_FRQ
#define SYS_CLK_I2C_FRQ       SYS_FRQ
#define SYS_CLK_ADC_FRQ       SYS_FRQ

/* Peripheral clocks with own clock selection */
/* @todo: consider clock selection configuration */
#define SYS_CLK_CLKOUT_FRQ    (SYS_FRQ_CLKOUT_BASE/SYS_CLK_CLKOUT_DIV)


#endif




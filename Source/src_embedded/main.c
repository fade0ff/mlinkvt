/** MLinkVT
	Voltage and temperature monitoring for LiPos with Multiplex Sensorbus

	----------------------------------------------------------
	Copyright 2017 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/


#include "global.h"
#include "sys.h"
#include "prc.h"
#include "pin.h"
#include "priowrap.h"
#include "dma.h"
#include "i2c.h"
#include "tmp102.h"
#include "uart.h" // include after dma.h to allow channel remapping hack
#include "adc.h"
#include "mrt.h"
#include "crc.h"
#include "nvm.h"
#include "systick.h"
#include "system_LPC8xx.h"
#include <string.h>
#include <stdlib.h>

#define NUM_VOLTAGES 5
#define MRT_DELAY MRT_INTVAL_INTERVAL(SYS_FRQ_US(300))   | MRT_INTVAL_LOAD_FORCE

#define INIT_ST     0
#define RECEIVE_ST  1
#define TRANSMIT_ST 2

#define ADR_READ_CONFIG    0x76
#define ADR_WRITE_CONFIG   0x64
#define ADR_CONFIG_WRITTEN 0x6F
#define ADR_FAILURE        0xFF


// Voltage divider is 4.7/(33+4.7) = 47/377, the inverse 47/377 is roughly 8 (8.0212766)
// Taking into account the 3V voltage reference, this means a maximum voltage of ~24V
#define VOLTAGE_MIN (4095/24/2) ///! roughly 0.5V (4095 == 24V before voltage divider)

#define SBUS_VOLTAGE 1
#define SBUS_TEMP    6

#define CALIBRATION_REPETITION 1000

typedef struct {
	u16 v_max;
	u16 v_min;
	u16 v_diff_max;
	u16 v_diff_min;
	s16 temp_max;
	s16 temp_min;
	u8  v_ch;
	u8  v_diff_ch;
	u8  temp_ch;
	u8  version;
} config_t;


s16 temperature;      ///! temperature
s16 temperature_sbus; ///! temperature (sensor bus resolution)
char str_buffer[16];
u8 tx_buffer[64];
u8 rx_buffer[64];
u8 alarm_tmp;
u8 alarm_volt_diff;
u8 alarm_volt;
u16 calibration_counter;
u16 v_diff;      ///! minimum difference of two channels
u16 v;           ///! maximum voltage of all channels
u16 v_diff_sbus; ///! minimum difference of two channels (sensor bus resolution)
u16 v_sbus;      ///! maximum voltage of all channels (sensor bus resolution)
u16 crc_config;


config_t config = {
	240,  // v_max = 24V
	30,   // v_min = 3V
	45,   // v_diff_max = 4.5V
	30,   // v_diff_min = 3V
	600,  // temp_max = 60°C
	-200, // temp_min = -20°C
	4,    // v_ch
	3,    // v_diff_ch
	2,    // tmp_ch
	0     // version
};

volatile u8 state = INIT_ST;
u16 voltage[NUM_VOLTAGES];

dma_entry_t dma_link         /** Link structure for ring buffer */  __attribute__ ((aligned(16)));

u16 calc_crc(u8 *data, u16 len) {
	int i;
	CRC_SetMode(CRC_MODE_POLY_CCITT);
	CRC_SetSeed(0xFFFF);
	for (i=0; i<len; i++)
		CRC_SetData8(data[i]);
	return CRC_GetSum();
}

void send_value(u8 adr, u8 type, s16 value, u8 alarm) {
	tx_buffer[0] = (adr<<4)|type;
	tx_buffer[1] = ((value<<1)&0xfe)|alarm;
	tx_buffer[2] = (value>>7);
	state = TRANSMIT_ST;
	rx_buffer[0] = 0xff;
	UART_Transmit(0, tx_buffer, 3);
}

void send_config(void) {
	int idx=0, idx2;
	tx_buffer[idx++] = 'm';
	tx_buffer[idx++] = 'l';
	tx_buffer[idx++] = 'i';
	tx_buffer[idx++] = 'n';
	tx_buffer[idx++] = 'k';
	tx_buffer[idx++] = 'v';
	tx_buffer[idx++] = 't';
	tx_buffer[idx++] = 0;
	tx_buffer[idx++] = sizeof(config);
	for (idx2=0; idx2<sizeof(config); idx2++)
		tx_buffer[idx++] = ((u8*)&config)[idx2];
	tx_buffer[idx++] = (u8)(crc_config&0xff);
	tx_buffer[idx++] = (u8)(crc_config>>8);
	state = TRANSMIT_ST;
	rx_buffer[0] = 0xff;
	UART_Transmit(0, tx_buffer, idx);
}

void sort_voltages(void) {
	int i,j;
	u16 tmp;

	for (j = 0; j < NUM_VOLTAGES-1; j++) {
		int idx_max = j;
		for (i = j+1; i < NUM_VOLTAGES; i++) {
			if (voltage[i] > voltage[idx_max])
				idx_max = i;
		}
		if(idx_max != j) {
			tmp = voltage[j];
			voltage[j] = voltage[idx_max];
			voltage[idx_max] = tmp;
		}
	}
}


void CommHandler(void) {
	u16 adr, count;
	count = UART_Receive(0, rx_buffer, sizeof(rx_buffer), UART_NOSCAN);
	if (state==RECEIVE_ST) {
		adr = rx_buffer[0];
		if (count==1) {
			// single byte commands
			if (adr < 16) {
				// a valid address request was received
				if (adr == config.temp_ch) {
					send_value(adr, SBUS_TEMP, temperature_sbus , alarm_tmp);
					alarm_tmp = 0;
				} else if (adr == config.v_ch) {
					send_value(adr, SBUS_VOLTAGE, v_sbus , alarm_volt);
					alarm_volt = 0;
				} else if (adr == config.v_diff_ch) {
					send_value(adr, SBUS_VOLTAGE, v_diff_sbus , alarm_volt_diff);
					alarm_volt_diff = 0;
				}
			} else if (adr == ADR_READ_CONFIG) {
				send_config();
			}
		} else {
			// multi byte commands
			if (adr == ADR_WRITE_CONFIG && count==(sizeof(config)+3)) {
				u16 crc_received = rx_buffer[count-2] | (rx_buffer[count-1]<<8);
				u16 crc_data = calc_crc(&rx_buffer[1], sizeof(config));
				if (crc_data == crc_received) {
					// write to NVRAM
					u8 status = NVM_Write(0, &rx_buffer[1], sizeof(config)+2);
					// return OK
					if (status != 0) {
						tx_buffer[0] = ADR_CONFIG_WRITTEN;
						memcpy((u8*)&config, &rx_buffer[1], sizeof(config));
						crc_config = crc_data;
					} else
						tx_buffer[0] = ADR_FAILURE;
					state = TRANSMIT_ST;
					rx_buffer[0] = 0xff;
					UART_Transmit(0, tx_buffer, 1);
				}
			}
		}
	} else {
		if (state == TRANSMIT_ST) {
			count = UART_Receive(0, rx_buffer, sizeof(rx_buffer), UART_NOSCAN);
			state = RECEIVE_ST;
		}
	}
}

void MRT_IRQHandler(void) {
	u32 flags = MRT_GetIntFlags();
	MRT_ClrIntFlags(flags);
	if (flags & MRT_IRQ_FLAG_CH0) {
		// time out happened after UART Receive
		CommHandler();
	}
}

void UART_RX_Handler(void) {
	// something was received, but don't react yet until the timeout has passed
	MRT_SetIntval(0, MRT_DELAY);
}

void SysTick_Handler(void) {
	// capture values
	temperature = TMP102_GetTemperature();
	voltage[0] = ADC_GetResult(ADC_CH_V1);
	voltage[1] = ADC_GetResult(ADC_CH_V2);
	voltage[2] = ADC_GetResult(ADC_CH_V3);
	voltage[3] = ADC_GetResult(ADC_CH_V4);
	voltage[4] = ADC_GetResult(ADC_CH_V5);
	// start next conversions
	TMP102_StartRead(TMP102_REG_TEMP);
	if (++calibration_counter >= CALIBRATION_REPETITION) {
		adc_start_calibration();
		calibration_counter = 0;
	} else
		adc_start_conversion(ADC_SEQA, ADC_CHANNELS);

	// sort voltages (in descending order)
	sort_voltages();
	v = voltage[0];

	// determine the minimum difference greater than a threshold
	u16 diff_min = 0xffff;
	u16 diff;
	int i;

	for (i=1; i<NUM_VOLTAGES; i++) {
		if (voltage[i] <= VOLTAGE_MIN)  // don't consider not connected voltages
			break;
		diff = voltage[i-1] - voltage[i]; // sorted: previous element is always larger (or equal)
		if (diff < diff_min)
			diff_min = diff;
	}
	if (diff_min != 0xffff)
		v_diff = diff_min;
	else
		v_diff = 0;

	// convert to sensor bus resolution
	temperature_sbus = (temperature*5+64)>>7;
	v_sbus = (v*58+494)/987;
	v_diff_sbus = (v_diff*58+494)/987;

	// limit checking
	if (temperature_sbus < config.temp_min || temperature_sbus > config.temp_max)
		alarm_tmp = 1;

	if (v_sbus < config.v_min || v_sbus > config.v_max)
		alarm_volt = 1;

	if (v_diff_sbus < config.v_diff_min || v_diff_sbus > config.v_diff_max)
		alarm_volt_diff = 1;

}

int main(void) {

	SystemInit();

	adc_init();
	adc_start_calibration();

	DMA_Init();
	DMA_IRQEnable(1);

	NVM_Init();
	u8 config_valid = 0;
	if (NVM_Read(0, tx_buffer, sizeof(config)+2) != 0) {
		// check crc
		u16 crc = calc_crc((u8*)&tx_buffer, sizeof(config));
		u16 crc_nvm = tx_buffer[sizeof(config)] | (tx_buffer[sizeof(config)+1]<<8);
		if (crc == crc_nvm) {
			memcpy((u8*)&config, &tx_buffer[0], sizeof(config));
			crc_config = crc;
			config_valid = 1;
		}
	}
	if (!config_valid)
		crc_config = calc_crc((u8*)&config, sizeof(config));

	PIN_Init();
	// disable switch matrix and IO config to save power
	SYSCON_DisableClockControl(SYS_CLK_SWM|SYS_CLK_IOCON);

	I2C_Init();

	TMP102_StartRead(TMP102_REG_TEMP);

	while (!adc_calibration_finished()); // needs about 290µs
	adc_start_conversion(ADC_SEQA, ADC_CHANNELS);

	// Init timebase
	// SysTick and PendSV are system exceptions - they don't need to be enabled
	NVIC_SetPriority(SysTick_IRQn, SYSTICK_IRQ_PRIO);   /* set to medium prio */
	SYSTICK_Init();                                     /* 10ms system tick */
	SYSTICK_Enable(1);
	SYSTICK_EnableIRQ(1);

	// Init MRT
	MRT_SetCtrl(0, MRT_CTRL_INTEN|MRT_CTRL_MODE_INT_ONCE);
	NVIC_SetPriority(MRT_IRQn, MRT_IRQ_PRIO);
	NVIC_EnableIRQ(MRT_IRQn);

	// Init UART
	UART_SetBaudRate(0, 38400);
	UART_Setup(0);

	state = RECEIVE_ST;

	while(1) {
		/* Sleep until next IRQ happens */
		__WFI();
	}
	return 0 ;
}


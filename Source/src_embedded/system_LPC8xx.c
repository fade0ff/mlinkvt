/**************************************************************************//**
 * @file     system_LPC8xx.c
 * @brief    CMSIS Device System Source File for
 *           NXP LPC81x Device Series
 * @version  V1.10
 * @date     19. August 2014
 *
 * @note
 * Copyright (C) 2014 ARM Limited. All rights reserved.
 *
 * @par
 * ARM Limited (ARM) is supplying this software for use with Cortex-M
 * processor based microcontrollers.  This file can be freely distributed
 * within development tools that are supporting such ARM based processors.
 *
 * @par
 * THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 * OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 * ARM SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL, OR
 * CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 *
 ******************************************************************************/

#include <stdint.h>
#include "global.h"
#include "sys.h"


/*----------------------------------------------------------------------------
  Clock Variable definitions
 *----------------------------------------------------------------------------*/
uint32_t SystemCoreClock = SYS_FRQ_CORE;      /* System Clock Frequency     */


/*----------------------------------------------------------------------------
  Clock functions
 *----------------------------------------------------------------------------*/
void SystemCoreClockUpdate (void)               /* Get Core Clock Frequency   */
{
  uint32_t wdt_osc = 0;

  /* Determine clock frequency according to clock register values             */
  switch ((LPC_SYSCON->WDTOSCCTRL >> 5) & 0x0F) {
    case 0:  wdt_osc =       0; break;
    case 1:  wdt_osc =  600000; break;
    case 2:  wdt_osc = 1050000; break;
    case 3:  wdt_osc = 1400000; break;
    case 4:  wdt_osc = 1750000; break;
    case 5:  wdt_osc = 2100000; break;
    case 6:  wdt_osc = 2400000; break;
    case 7:  wdt_osc = 2700000; break;
    case 8:  wdt_osc = 3000000; break;
    case 9:  wdt_osc = 3250000; break;
    case 10: wdt_osc = 3500000; break;
    case 11: wdt_osc = 3750000; break;
    case 12: wdt_osc = 4000000; break;
    case 13: wdt_osc = 4200000; break;
    case 14: wdt_osc = 4400000; break;
    case 15: wdt_osc = 4600000; break;
  }
  wdt_osc /= (((LPC_SYSCON->WDTOSCCTRL & 0x1F) + 1) << 1);

  switch (LPC_SYSCON->MAINCLKSEL & 0x03) {
    case 0:                                           /* Internal RC oscillator     */
      SystemCoreClock = SYS_FRQ_IRC;
      break;
    case 1:                                           /* Input Clock to System PLL  */
      switch (LPC_SYSCON->SYSPLLCLKSEL & 0x03) {
        case 0:                                       /* Internal RC oscillator     */
          SystemCoreClock = SYS_FRQ_IRC;
          break;
        case 1:                                       /* System oscillator          */
          SystemCoreClock = SYS_FRQ_OSC;
          break;
        case 2:                                       /* Reserved                   */
          SystemCoreClock = 0;
          break;
      }
      break;
    case 2:                                           /* WDT Oscillator             */
      SystemCoreClock = wdt_osc;
      break;
    case 3:                                           /* System PLL Clock Out       */
      switch (LPC_SYSCON->SYSPLLCLKSEL & 0x03) {
        case 0:                                       /* Internal RC oscillator     */
          SystemCoreClock = SYS_FRQ_IRC * ((LPC_SYSCON->SYSPLLCTRL & 0x01F) + 1);
          break;
        case 1:                                       /* System oscillator          */
          SystemCoreClock = SYS_FRQ_OSC * ((LPC_SYSCON->SYSPLLCTRL & 0x01F) + 1);
          break;
        case 2:                                       /* Reserved                   */
          SystemCoreClock = 0;
          break;
      }
      break;
  }

  SystemCoreClock /= LPC_SYSCON->SYSAHBCLKDIV;

}


/**
 * Initialize the system
 *
 * @param  none
 * @return none
 *
 * @brief  Setup the microcontroller system.
 */
void SystemInit (void) {
	u32 i;

	(void)i;
	/* System clock to the IOCON & the SWM need to be enabled or
     most of the I/O related peripherals won't work. */
	LPC_SYSCON->SYSAHBCLKCTRL = SYSAHBCLKCTRL_CFG;

#if SYS_FRQ_USE_SYSOSC
    LPC_IOCON->PIO0_8         &= ~(3 <<  3);          /* no pull-down/pull-up       */
    LPC_IOCON->PIO0_9         &= ~(3 <<  3);          /* no pull-down/pull-up       */
    LPC_SWM->PINENABLE0       &= ~(3 <<  4);          /* enable XTALIN/XTALOUT func.*/
	/* Power up main oscillator */
	SYSCON_PowerUp(SYSCON_POWERDOWN_SYSOSC_PD);
	SYSCON_SetOscCtrl((SYS_FRQ_OSC<20000000)?SYSCON_OSCCTL_FREQRANGE_20MHZ:SYSCON_OSCCTL_FREQRANGE_25MHZ);
	for (i = 0; i < 200; i++)
		__NOP();                                      /* Wait for osc to stabilize  */
#endif

#if SYS_FRQ_USE_SYS_PLL
	SYSCON_SetPLLSource(SYS_PLL_CLKSRC_CFG);
	while (!(LPC_SYSCON->SYSPLLCLKUEN & 0x01)) __NOP(); /* Wait Until Updated         */
	SYSCON_PLL_Setup(SYS_MSEL_CFG,SYS_PSEL_CFG);
	SYSCON_PowerUp(SYSCON_POWERDOWN_SYSPLL_PD);
	while (!SYSCON_PLL_Locked()) __NOP();
#endif

#if SYS_FRQ_USE_WDT
	SYSCON_SetWdtOscControl(SYS_FRQ_WDT_FRQSEL, SYS_FRQ_WDT_DIV);
	SYSCON_PowerUp(SYSCON_POWERDOWN_WDTOSC_PD);
	for (i = 0; i < 200; i++)
		__NOP();                /* Wait for osc to stabilize  */
#endif

	SYSCON_SelectSystemClock(SYS_MAIN_CLKSEL_CFG);
	while (!(LPC_SYSCON->MAINCLKUEN & 0x01)) __NOP();   /* Wait Until Updated         */

	LPC_SYSCON->UARTCLKDIV   = SYS_CLK_UART_DIV;
	LPC_SYSCON->CLKOUTDIV    = SYS_CLK_CLKOUT_DIV;
	LPC_SYSCON->IOCONCLKDIV0 = SYS_CLK_IOCON_DIV0;
	LPC_SYSCON->IOCONCLKDIV1 = SYS_CLK_IOCON_DIV1;
	LPC_SYSCON->IOCONCLKDIV2 = SYS_CLK_IOCON_DIV2;
	LPC_SYSCON->IOCONCLKDIV3 = SYS_CLK_IOCON_DIV3;
	LPC_SYSCON->IOCONCLKDIV4 = SYS_CLK_IOCON_DIV4;
	LPC_SYSCON->IOCONCLKDIV5 = SYS_CLK_IOCON_DIV5;
	LPC_SYSCON->IOCONCLKDIV6 = SYS_CLK_IOCON_DIV6;

	SYSCON_SetSysClockDivider(SYS_CLK_DIV);


}

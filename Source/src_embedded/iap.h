/** In application programming
	Definitions for calling IAP ROM functions
	LPC824 ARM Cortex M0+
	----------------------------------------------------------
	Copyright 2017 Volker Oth
	Licensed under the Creative Commons Attribution 4.0 license
	http://creativecommons.org/licenses/by/4.0/

	Software is distributed on an "AS IS" BASIS, WITHOUT
	WARRANTIES OF ANY KIND, either express or implied.
*/

#ifndef IAP_H
#define IAP_H
#include "global.h"

#define IAP_LOCATION 0x1fff1ff1

#define IAP_STAT_CMD_SUCCESS           0 ///! Command is executed successfully
#define IAP_STAT_INVALID_COMMAND       1 ///! Invalid command.
#define IAP_STAT_SRC_ADDR_ERROR        2 ///! Source address is not on a word boundary.
#define IAP_STAT_DST_ADDR_ERROR        3 ///! Destination address is not on a correct boundary.
#define IAP_STAT_SRC_ADDR_NOT_MAPPED   4 ///! Source address is not mapped in the memory map.
#define IAP_STAT_DST_ADDR_NOT_MAPPED   5 ///! Destination address is not mapped in the memory
#define IAP_STAT_COUNT_ERROR           6 ///! Byte count is not multiple of 4 or is not a permitted  value.
#define IAP_STAT_INVALID_SECTOR        7 ///! Sector number is invalid.
#define IAP_STAT_SECTOR_NOT_BLANK      8 ///! Sector is not blank.
#define IAP_STAT_SECTOR_NOT_PREPARED   9 ///! Command to prepare sector for write operation was  not executed.
#define IAP_STAT_SRC_DEST_EQUAL       10 ///! COMPARE_ERROR Source and destination data is not same.
#define IAP_STAT_BUSY                 11 ///! Flash programming hardware interface is busy.
#define IAP_STAT_ERR_ISP_IRC_OFF      17 ///! IRC is turned off. The IRC must be running to use the IAP calls.
#define IAP_STAT_ERR_ISP_FLASH_OFF    18 ///! Flash is powered down
#define IAP_STAT_ERR_ISP_FLASH_CLOCK  27 ///! Flash clock is off.

#define IAP_CMD_PREPARE_SECTOR        50 ///! Prepare sector(s) for write operation
#define IAP_CMD_COPY_RAM_TO_FLASH     51 ///! Copy RAM to flash
#define IAP_CMD_ERASE_SECTOR          52 ///! Erase Sector
#define IAP_CMD_BLANK_CHECK_SECTOR    53 ///! Blank check sector
#define IAP_CMD_READ_PART_ID          54 ///! Read Part Identification number
#define IAP_CMD_READ_BOOT_VERSION     55 ///! Read Boot code version number
#define IAP_CMD_COMPARE               56 ///! Compare address range(s)
#define IAP_CMD_REINVOKE_ISP          57 ///! Reinvoke ISP
#define IAP_CMD_READ_UID              58 ///! Read UID
#define IAP_CMD_ERASE_PAGE            59 ///! Erase page

/* #   pages    start        end
   0   0  -  15 0x00000000 - 0x000003FF
   1   16 -  31 0x00000400 - 0x000007FF
   2   32 -  47 0x00000800 - 0x00000BFF
   3   48 -  63 0x00000C00 - 0x00000FFF
   4   64 -  79 0x00001000 - 0x000013FF
   5   80 -  95 0x00001400 - 0x000017FF
   6   96 - 111 0x00001800 - 0x00001BFF
   7  112 - 127 0x00001C00 - 0x00001FFF
   8  128 - 143 0x00002000 - 0x000023FF
   9  144 - 159 0x00002400 - 0x000027FF
   10 160 - 175 0x00002800 - 0x00002BFF
   11 176 - 191 0x00002C00 - 0x00002FFF
   12 192 - 207 0x00003000 - 0x000033FF
   13 208 - 223 0x00003400 - 0x000037FF
   14 224 - 239 0x00003800 - 0x00003BFF
   15 240 - 255 0x00003C00 - 0x00003FFF
   16 256 - 271 0x00004000 - 0x000043FF
   17 272 - 287 0x00004400 - 0x000047FF
   18 288 - 303 0x00004800 - 0x00004BFF
   19 304 - 319 0x00004C00 - 0x00004FFF
   20 320 - 335 0x00005000 - 0x000053FF
   21 336 - 351 0x00005400 - 0x000057FF
   22 352 - 367 0x00005800 - 0x00005BFF
   23 368 - 383 0x00005C00 - 0x00005FFF
   24 384 - 399 0x00006000 - 0x000063FF
   25 400 - 415 0x00006400 - 0x000067FF
   26 416 - 431 0x00006800 - 0x00006BFF
   27 432 - 447 0x00006C00 - 0x00006FFF
   28 448 - 463 0x00007000 - 0x000073FF
   29 464 - 479 0x00007400 - 0x000077FF
   30 480 - 495 0x00007800 - 0x00007BFF
   31 496 - 511 0x00007C00 - 0x00007FFF
*/

#define IAP_GET_SECTOR(adr) ((adr)/0x400)
#define IAP_GET_PAGE(adr)   ((adr)/64)

#define IAP_GET_SECT_ADR(sector)  ((sector)*0x400)
#define IAP_GET_PAGE_ADR(sector)  ((page)*64)

typedef void (*IAP)(u32 iap_cmd[], u32 iap_res[]);

extern u8 iap_prepare_sectors(u8 start_sector, u8 end_sector);
extern u8 iap_copy_to_flash(u32 src_adr, u32 dst_adr, u16 count);
extern u8 iap_erase_sectors(u8 start_sector, u8 end_sector);
extern u8 iap_blank_check_sectors(u8 start_sector, u8 end_sector, u32 *offset, u32 *contents);
extern u8 iap_read_part_id(u32 *id);
extern u8 iap_read_boot_version(u16 *version);
extern u8 iap_compare(u32 src_adr, u32 dst_adr, u16 count, u32* offset);
extern u8 iap_invoke_isp(void);
extern u8 iap_read_uid(u32 *uid);
extern u8 iap_erase_pages(u8 start_page, u8 end_page);

#endif

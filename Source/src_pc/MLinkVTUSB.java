/*
 * Copyright 2017 Volker Oth
 *
 * Licensed under the Creative Commons Attribution 4.0 license
 * http://creativecommons.org/licenses/by/4.0/
 *
 * Software is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OF ANY KIND, either express or implied.
 */

import javax.swing.UIManager;

/**
 * Contains main class and command line functionality.
 * The core functions are located in {@link Core}, the GUI in {@link MainFrame}.
 *
 * @author Volker Oth
 */
class MLinkVTUSB {

	/**
	 * Main function.
	 * @param args Parameters passed from the command line
	 */
	public static void main(String[] args) throws Exception {
		// Set "Look and Feel" to system default
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) { /* don't care */ }
		// Apple menu bar for MacOS
		System.setProperty("apple.laf.useScreenMenuBar", "true");
		new MainFrame();
	}

}


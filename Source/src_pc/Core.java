/*
 * Copyright 2013 Volker Oth
 *
 * Licensed under the Creative Commons Attribution 4.0 license
 * http://creativecommons.org/licenses/by/4.0/
 *
 * Software is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OF ANY KIND, either express or implied.
 */

import java.awt.Image;
import java.awt.Toolkit;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JOptionPane;

import com.fazecast.jSerialComm.SerialPort;


/**
 * Core class for NGenUSB.
 * Used to store application data and to implement the main functionality.
 * @author Volker Oth
 *
 */
public class Core
{

	/** Program name */
	final static String progName = "MTLinkVT USB";
	/** Program name to display in the main window, in the log, in the command line etc.  */
	final static String progNameVer = progName + " 1.0.0";
	/** Revision and release date for display in the log windows etc. */
	final static String authorDate = "V. Oth 10/2017";
	/** ID string in the answer of the device */
	final static String configID = "mlinkvt";

	/** COM buffer size */
	private static final int comBufferSize = 256;
	/** Config data size - has to fit the size used by the device */
	private static final int configDataSize = 16;
	/** Config read size - has to fit the size used by the device (ID string (7 bytes) + "\0" + size (8bit) + config (16 bytes) + CRC (16bit)) */
	private static final int configReadSize = configDataSize + configID.length()+1+1+2;
	
	private final static int crc16_poly = 0x1021;
	private final static int crc16_seed = 0xffff;
	
	/** Icon 16x16 */
	final static Image iconImage16 = Toolkit.getDefaultToolkit().getImage(MainFrame.class.getClassLoader().getResource("icon_16.png"));
	/** Icon 32x32 */
	final static Image iconImage32 = Toolkit.getDefaultToolkit().getImage(MainFrame.class.getClassLoader().getResource("icon_32.png"));
	/** Icon 48x48 */
	final static Image iconImage48 = Toolkit.getDefaultToolkit().getImage(MainFrame.class.getClassLoader().getResource("icon_48.png"));
	/** Icon 64x64 */
	final static Image iconImage64 = Toolkit.getDefaultToolkit().getImage(MainFrame.class.getClassLoader().getResource("icon_64.png"));
	/** Icon 128x128 */
	final static Image iconImage128 = Toolkit.getDefaultToolkit().getImage(MainFrame.class.getClassLoader().getResource("icon_128.png"));
	/** Array of Icons */
	final static ArrayList<Image> iconImages = new ArrayList<Image>(Arrays.asList(iconImage16,iconImage32,iconImage48,iconImage64,iconImage128));


	/** Read configuration */
	final static int CMD_READ_CONFIG      =  0x76;
	/** Write configuration */
	final static int CMD_WRITE_CONFIG     =  0x64;

	/** No fault */
	final static int WRITE_OK             = 0x6F;
	/** No fault */
	final static int WRITE_FAILED         = 0xFF;

	
	/** Program states */
	public enum State {
		/** Program is being initialized */
		INIT_ST(0),
		/** Scan channels for data and devices */
		SCANNING(1),
		/** Device detected */
		DETECTED(2),
		/** write config */
		WRITE_CONFIG(3),
		/** write config */
		WRITE_CONFIG_CHECK(4),
		/** Generator state is unknown */
		UNKNOWN(5);

		/** internal variable to store integer value of state */
		private int value;

		/** Constructor used to define enum entries with predefined integer values */
		private State(int value) {
			this.value = value;
		}
		
		/** Get state as integer value
		 * @return integer value of enum member
		 */
		public int getValue() {
			return value;
		}

	}

	/** COM port */
	private static SerialPort comPort;
	private static SerialPort[] comPorts;
	private static byte[] writeBuffer = new byte[comBufferSize];
	private static byte[] readBuffer = new byte[comBufferSize];
	/** Device state */
	private static State state = State.UNKNOWN;

	/** Name of data/setup file */
	public static String fileName;
	/** Reference to GUI main frame */
	public static MainFrame mainFrame;

	/** Version number */
	private static int version;

	/** Temperature channel */
	private static int tempChannel;	
	/** Temperature minimum */
	private static int tempMin;
	/** Temperature maximum */
	private static int tempMax;
	
	/** Voltage channel */
	private static int voltChannel;
	/** Voltage minimum */
	private static int voltMin;
	/** Voltage maximum */
	private static int voltMax;
	
	/** Voltage delta channel */
	private static int voltDeltaChannel;
	/** Voltage minimum */
	private static int voltDeltaMin;
	/** Voltage maximum */
	private static int voltDeltaMax;

	
	private static int channel;
	
	private static boolean ready;

	
	private final static DecimalFormatSymbols dfSym = new DecimalFormatSymbols();
	private static DecimalFormat dot3 = null;
	final static Pattern regTrim = Pattern.compile("\\s*([^\\s]+)\\s*");
	
	
	
	/**
	 * Read byte from a buffer from position index
	 * @param buffer Byte array
	 * @param index Index to read from
	 * @return Integer value of byte buffer[index]
	 * @throws ArrayIndexOutOfBoundsException
	 */
	public static int getByte(final byte buffer[], final int index) throws ArrayIndexOutOfBoundsException {
		return buffer[index] & 0xff;
	}

	/**
	 * Read (little endian) word from a buffer from position index
	 * @param buffer Byte array
	 * @param index Index to read from
	 * @return Integer value of word starting at buffer[index] (index points at least significant byte)
	 * @throws ArrayIndexOutOfBoundsException
	 */
	public static int getWord(final byte buffer[], final int index) throws ArrayIndexOutOfBoundsException {
		return (buffer[index] & 0xff) | ((buffer[index+1] & 0xff)<<8);
	}
	
	/**
	 * Write byte to buffer[index]
	 * @param buffer Byte array
	 * @param index Index to write to
	 * @param val Integer value of byte to write
	 * @throws ArrayIndexOutOfBoundsException
	 */
	public static void setByte(final byte buffer[], final int index, final int val) throws ArrayIndexOutOfBoundsException {
		buffer[index]   = (byte)(val);
	}

	/**
	 * Write (little endian) word to buffer[index] (index points at least significant byte)
	 * @param buffer Byte array
	 * @param index Index to write to
	 * @param val Integer value of word to write
	 * @throws ArrayIndexOutOfBoundsException
	 */
	public static void setWord(final byte buffer[], final int index, final int val) throws ArrayIndexOutOfBoundsException {
		buffer[index] = (byte)(val);
		buffer[index+1]   = (byte)(val>>8);
	}

	
	/**
	 * Remove trailing and leading spaces from a string
	 * @param s String to process
	 * @return String without leading and trailing spaces
	 */
	public static String trim(final String s) {
		String t=null;
		Matcher m = regTrim.matcher(s);
		if (m.matches())
			t = m.group(1);
		if (t==null)
			return "";
		else
			return t;
	}

	
	/**
	 * Convert String to double
	 * @param s String containing double
	 * @return Double value or Double.MIN_VALUE if no valid numerical value
	 */
	public static double getDouble(final String s) {
		try {
			return Double.parseDouble(trim(s));
		} catch (NumberFormatException ex) {
			return Double.MIN_VALUE;
		}
	}
	
	public static String strVolt(int val) {
		return formatDouble(val/10.0);
	}
	
	public static String strTemp(int val) {
		return formatDouble(val/10.0);
	}

	
	public static int getVolt(String s) {
		return (int)Math.round(getDouble(s)*10.0);
	}
	
	public static int getTemp(String s) {
		return (int)Math.round(getDouble(s)*10.0);
	}
	
	public static boolean voltInRange(int volt) {
		return (volt >= 0 && volt <= 600);
	}
	
	public static boolean tempInRange(int temp) {
		return (temp >= -250 && temp <= 7000);
	}

	public static SerialPort getComPort() {
		return comPort;
	}
	
	public static int getTempChannel() {
		return tempChannel;
	}

	public static void setTempChannel(int ch) {
		tempChannel = ch;
	}

	public static int getTempMin() {
		return tempMin;
	}

	public static void setTempMin(int min) {
		tempMin = min;
	}

	public static int getTempMax() {
		return tempMax;
	}

	public static void setTempMax(int max) {
		tempMax = max;
	}

	public static int getVoltChannel() {
		return voltChannel;
	}

	public static void setVoltChannel(int ch) {
		voltChannel = ch;
	}

	public static int getVoltMin() {
		return voltMin;
	}

	public static void setVoltMin(int min) {
		voltMin = min;
	}

	public static int getVoltMax() {
		return voltMax;
	}

	public static void setVoltMax(int max) {
		voltMax = max;
	}

	public static int getVoltDeltaChannel() {
		return voltDeltaChannel;
	}

	public static void setVoltDeltaChannel(int ch) {
		voltDeltaChannel = ch;
	}

	public static int getVoltDeltaMin() {
		return voltDeltaMin;
	}

	public static void setVoltDeltaMin(int min) {
		voltDeltaMin = min;
	}

	public static int getVoltDeltaMax() {
		return voltDeltaMax;
	}

	public static void setVoltDeltaMax(int max) {
		voltDeltaMax = max;
	}
	
	/**
	 * Get Core ready state.
	 * @return True if the Core is ready
	 */
	public static boolean isReady() {
		return ready;
	}

	/**
	 * Set Core ready state.
	 * @param r true if the Core is ready
	 */
	public static void setReady(final boolean r) {
		ready = r;
	}


	/**
	 * Get generator state that was read from device during last HID in report.
	 * @return generator state
	 */
	public static State getState() {
		return state;
	}
	
	public static void triggerReadConfig() {
		if (state == State.DETECTED)
			state = State.SCANNING;
	}
	
	public static void triggerWriteConfig() {
		if (state == State.DETECTED)
			state = State.WRITE_CONFIG;
	}


	/**
	 * Initialize the core component. Do this before using any other functions.
	 * @param c just any non-static object
	 */
	public static void init(Object c) {
		comPorts = SerialPort.getCommPorts();		
		ready = true;
	}

	public static int comPortNumber() {
		return comPorts.length;
	}
	
	public static String comPortString(int index) {
		return comPorts[index].getDescriptivePortName();
	}
	
	/**
	 * Check if COM port is connected
	 * @return true if connected, else false
	 */
	synchronized public static boolean isConnected() {
		return comPort != null && comPort.isOpen();
	}

	/**
	 * Connect to COM port
	 * @return true if connect was successful, else false
	 */
	synchronized public static boolean connect(int index) {
		try {
			comPort = comPorts[index];
			comPort.openPort();
			comPort.setComPortParameters(38400, 8, 1, 0);
			state = State.SCANNING;
		} catch (Exception e) {
			comPort = null;
			state = State.INIT_ST;
		}
		return (comPort != null);
	}

	/**
	 * Disconnect HID.
	 */
	synchronized public static void disconnect() {
		if (comPort != null)
			try {
				state = State.UNKNOWN;
				comPort.closePort();
				comPort = null;
				state = State.INIT_ST;
			} catch (Exception e) {
				System.err.println(e.getMessage());
				e.printStackTrace();
			}
	}

	/**
	 * Shutdown core functionality, save settings, disconnect HID.
	 */
	public static void exit() {
		disconnect();
	}

	/**
	 * Get program name (excluding version).
	 * @return Program name (excluding version)
	 */
	public static String getProgName() {
		return progName;
	}

	/**
	 * Get program name (including version).
	 * @return Program name (including version)
	 */
	public static String getProgVerName() {
		return progNameVer;
	}

	/**
	 * Get author and date info.
	 * @return Author and date info
	 */
	public static String getAuthorDate() {
		return authorDate;
	}

	/** Format double as string in the form "xx.yyy"
	 * @param d Double value
	 * @return Formatted string
	 */
	public static String formatDouble(final double d) {
		if (dot3 == null) {
			dfSym.setDecimalSeparator('.');
			dot3 = new DecimalFormat("##.#", dfSym);
		}
		return dot3.format(d);
	}

	public static void scan() {
		if (comPort != null && comPort.isOpen()) {
			int numRead = comPort.bytesAvailable();
			if (numRead > 0) {					
				numRead = comPort.readBytes(readBuffer, readBuffer.length);
				if (numRead == 4 && readBuffer[0] == channel /* echo of own tx */) {
					// analyze data
					analyzeData(readBuffer[1]&0xff, readBuffer[2]&0xff, readBuffer[3]&0xff);
				} else if (numRead == 1 && readBuffer[0] == channel) {
					Core.mainFrame.setTable(channel, "", "");
				} else if (readBuffer[0] == CMD_READ_CONFIG) {						
					if (readConfig(Arrays.copyOfRange(readBuffer, 1, numRead))) {
						if (state != State.DETECTED)
							mainFrame.setConfig();
						state = State.DETECTED;
					} else {
						state = State.SCANNING;
						mainFrame.disableConfig();
					}
				} else if (readBuffer[0] == CMD_WRITE_CONFIG) {
					state = State.SCANNING; // read new config
					if (numRead == configDataSize+4) {
						if (readBuffer[numRead-1] == WRITE_OK) 
							JOptionPane.showMessageDialog(mainFrame,"Configuration written successfully!","Success!", JOptionPane.INFORMATION_MESSAGE);
						else
							JOptionPane.showMessageDialog(mainFrame,"Writing configuration failed!","Error!", JOptionPane.WARNING_MESSAGE);
					}
				}
			}
			if (state == State.WRITE_CONFIG) {				
				writeConfig();
				state = State.WRITE_CONFIG_CHECK;
			} else if (state == State.SCANNING || state == State.DETECTED){
				if (++channel > 16)
					channel = 0;
				if (channel <= 15)
					writeBuffer[0] = (byte)channel;
				else 
					writeBuffer[0] = CMD_READ_CONFIG; // read ID
				comPort.writeBytes(writeBuffer, 1);
			}
		}
	}

	
	static boolean readConfig(byte data[]) {
		int ofs;
		int value;
		// check length of answer
		if (data.length != configReadSize)
			return false;
		// check ID string
		String str;
		try {
			str = new String(data, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			str = "";
		}
		if (!str.startsWith(configID)) 
			return false;
		// check embedded config length
		ofs = configID.length()+1; // behind 0 terminated ID string 
		value = getByte(data, ofs); ofs ++;
		if (value != configDataSize)
			return false;
		// check CRC
		value = getWord(data, ofs+configDataSize);
		int crc = crc16(Arrays.copyOfRange(data, ofs, ofs+configDataSize));
		if (crc != value)
			return false;
		// read config
		voltMax = getWord(data, ofs); ofs+=2;
		voltMin = getWord(data, ofs); ofs+=2;
		voltDeltaMax = getWord(data, ofs); ofs+=2;
		voltDeltaMin = getWord(data, ofs); ofs+=2;
		tempMax = (short)getWord(data, ofs); ofs+=2;
		tempMin = (short)getWord(data, ofs); ofs+=2;
		voltChannel = getByte(data, ofs); ofs++;
		voltDeltaChannel = getByte(data, ofs); ofs++;
		tempChannel = getByte(data, ofs); ofs++;
		version = getByte(data, ofs); ofs++;
		return true;
	}
	
	static void writeConfig() {
		int ofs = 0;
		// write command
		setByte(writeBuffer, ofs, CMD_WRITE_CONFIG); ofs++;
		// write config
		setWord(writeBuffer, ofs, voltMax); ofs+=2;
		setWord(writeBuffer, ofs, voltMin); ofs+=2;
		setWord(writeBuffer, ofs, voltDeltaMax); ofs+=2;
		setWord(writeBuffer, ofs, voltDeltaMin); ofs+=2;
		setWord(writeBuffer, ofs, (short)tempMax); ofs+=2;
		setWord(writeBuffer, ofs, (short)tempMin); ofs+=2;
		setByte(writeBuffer, ofs, voltChannel); ofs++;
		setByte(writeBuffer, ofs, voltDeltaChannel); ofs++;
		setByte(writeBuffer, ofs, tempChannel); ofs++;
		setByte(writeBuffer, ofs, version); ofs++;
		// write CRC
		int crc = crc16(Arrays.copyOfRange(writeBuffer, 1, ofs));
		setWord(writeBuffer, ofs, crc); ofs+=2;		
		comPort.writeBytes(writeBuffer, ofs);
	}
	
	static void analyzeData(int val0, int val1, int val2) {
		// 0: Adresse | Werteklasse (Adresse ist high nibble und Werteklasse low nibble)
		// 1: Lowbyte | Alarmflag   (die oberen 7bit sind die untersten 7bit des Wertes und das Alarmflag ist das unterste Bit)
		// 2: Highbyte              (das oberste Bit ist das Vorzeichen gefolgt von den oberen 7bit des Wertes).
		int adr = val0>>4;
		int vclass = val0&0xf;
		int alarm = val1&1;
		int value = (val1>>1)|(val2<<7);
		// check and correct sign
		if ((value & 0x4000) != 0) 
			value = (value&0x3FFF)-0x4000;
		double val = value;
		String strValue;
		String unit;
		switch (vclass) {
			case 0:
				unit = "special";
				break;
			case 1:
				unit = "V";
				val = val/10.0;
				break;
			case 2:
				unit = "A";
				val = val/10.0;
				break;
			case 3:
				unit = "m/s";
				val = val/10.0;
				break;				
			case 4:
				unit = "km/h";
				val = val/10.0;
				break;				
			case 5:
				unit = "1/min";
				val = val/10.0; // ????????
				break;				
			case 6:
				unit = "�C";
				val = val/10.0;
				break;
			case 7:
				unit = "�";
				val = val/10.0;
				break;
			case 8:
				unit = "m";
				break;
			case 9:
				unit = "% (Fuel)";
				break;
			case 10:
				unit = "% (LQI)";
				break;
			case 11:
				unit = "mAh";
				break;
			case 12:
				unit = "mL";
				break;
			case 13:
				unit = "km";
				val = val/10.0;
				break;
			default:
				unit = "???";		
		}
		if (alarm != 0)
			unit += " !";
		if (value == 0x8000)
			strValue = "-";
		else
			strValue = formatDouble(val);
		mainFrame.setTable(adr, strValue, unit);
	}
	
	static int crc16(byte data[], int poly, int seed) {
		int crc = seed;
		for (int idx=0; idx<data.length; idx++) {
			int d = (data[idx]&0xff)<<8;
			for (int idx2=0;idx2<8;idx2++) {
				crc <<= 1;
				d <<= 1;
				if ( ((d^crc) & 0x10000) != 0 ) {
					crc ^= poly;
				}
			}      
		}
		return crc & 0xffff;
	}
	
	static int crc16(byte data[]) {
		return crc16(data, crc16_poly, crc16_seed);
	}

}
import java.awt.Color;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;

import net.miginfocom.swing.MigLayout;


public class MainFrame extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTable table;
	@SuppressWarnings("rawtypes")
	private JComboBox comboBoxComm;
	private JButton btnConnect;

	private Timer timer;
	private Font defaultFont;
		
	private DefaultTableModel tableModel; 
	private boolean ready = false;
	private JPanel ConfigPanel;
	private JPanel DeltaVoltagePanel;
	private JPanel VoltagePanel;
	private JPanel TemperaturePanel;
	private JLabel lblDeltaChannel;
	private JLabel lblDeltaMin;
	private JLabel lblDeltaMax;
	@SuppressWarnings("rawtypes")
	private JComboBox comboBoxDelta;
	private JTextField txtDeltaMin;
	private JTextField txtDeltaMax;
	private JLabel lblVoltChannel;
	private JLabel lblVoltMin;
	private JLabel lblVoltMax;
	@SuppressWarnings("rawtypes")
	private JComboBox comboBoxVolt;
	private JTextField txtVoltMin;
	private JTextField txtVoltMax;
	private JLabel lblTempChannel;
	private JLabel lblTempMin;
	private JLabel lblTempMax;
	@SuppressWarnings("rawtypes")
	private JComboBox comboBoxTemp;
	private JTextField txtTempMin;
	private JTextField txtTempMax;
	private JPanel ConnectPanel;
	private JButton btnReadConfig;
	private JButton btnWriteConfig;

	
	/** Background color for errors */
	private static final Color errBgnd = new Color(0xffe1acac);
	/** Background color for OK/Not set */
	private static final Color okPendingBgnd = new Color(0xfface1ac);
	/** Background color for OK/Set */
	private static final Color okSetBgnd = UIManager.getColor("TextField.background");
	/**
	 * Create the frame.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public MainFrame()  throws Exception {
		Core.mainFrame = this;
		Core.init(this);
		this.setSize(800,600);
		this.setMinimumSize(this.getSize());
		
		this.addWindowListener(new java.awt.event.WindowAdapter() {
			@Override
			public void windowClosing(java.awt.event.WindowEvent e) {
				exit();
			}

			@Override
			public void windowClosed(java.awt.event.WindowEvent e) {
				exit();
			}
		});
	
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		Point p = ge.getCenterPoint();
		p.x -= this.getWidth()/2;
		p.y -= this.getHeight()/2;
		this.setLocation(p);

		
		defaultFont = new JMenu().getFont();		

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new MigLayout("", "[grow][grow]", "[][]"));
		
		ConnectPanel = new JPanel();
		contentPane.add(ConnectPanel, "cell 0 0 2 1,grow");
		ConnectPanel.setLayout(new MigLayout("", "[][][grow][][]", "[]"));
		
		comboBoxComm = new JComboBox();
		ConnectPanel.add(comboBoxComm, "cell 0 0");
		comboBoxComm.setEditable(false);
		comboBoxComm.setFont(defaultFont);
		
		btnConnect = new JButton("Connect");
		ConnectPanel.add(btnConnect, "cell 1 0,alignx left");
		btnConnect.setFont(defaultFont);
		
		btnReadConfig = new JButton("Read Config");
		btnReadConfig.setFont(defaultFont);
		btnReadConfig.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Core.triggerReadConfig();
			}
		});
		ConnectPanel.add(btnReadConfig, "cell 3 0,alignx right");
		
		btnWriteConfig = new JButton("Write Config");
		btnWriteConfig.setFont(defaultFont);
		btnWriteConfig.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				readTextFields();
				readComboBoxes();
				Core.triggerWriteConfig();
			}
		});
		ConnectPanel.add(btnWriteConfig, "cell 4 0,alignx right");
		btnConnect.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (ready && !Core.isConnected()) {
					btnConnect.setText("Disconnect");
					Core.connect(comboBoxComm.getSelectedIndex());
					comboBoxComm.setEnabled(false);
				} else {
					Core.disconnect();
					btnConnect.setText("Connect");
					comboBoxComm.setEnabled(true);
				}
					
			}
		});
				


		String columnNames[]= {"Channel","Value","Unit"};
		tableModel = new DefaultTableModel(columnNames, 16);
		
		ConfigPanel = new JPanel();
		contentPane.add(ConfigPanel, "cell 0 1,grow");
		ConfigPanel.setLayout(new MigLayout("", "[grow]", "[grow][grow][grow]"));
		
		DeltaVoltagePanel = new JPanel();
		DeltaVoltagePanel.setBorder(new TitledBorder(null, "Delta Voltage", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		ConfigPanel.add(DeltaVoltagePanel, "cell 0 0,grow");
		DeltaVoltagePanel.setLayout(new MigLayout("", "[][grow]", "[][][]"));
		
		lblDeltaChannel = new JLabel("Channel");
		lblDeltaChannel.setFont(defaultFont);
		DeltaVoltagePanel.add(lblDeltaChannel, "cell 0 0,alignx trailing");
		
		comboBoxDelta = new JComboBox();
		comboBoxDelta.setFont(defaultFont);
		DeltaVoltagePanel.add(comboBoxDelta, "cell 1 0,growx");
		
		lblDeltaMin = new JLabel("Minimum");
		lblDeltaMin.setFont(defaultFont);
		DeltaVoltagePanel.add(lblDeltaMin, "cell 0 1,alignx trailing");
		
		txtDeltaMin = new JTextField();
		DeltaVoltagePanel.add(txtDeltaMin, "cell 1 1,growx");
		txtDeltaMin.setFont(defaultFont);
		txtDeltaMin.setColumns(10);
		txtDeltaMin.setToolTipText("Set minimum voltage (V)");
		txtDeltaMin.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (Core.isReady()) {
					Core.setReady(false);
					int v = Core.getVolt(txtDeltaMin.getText());
					if ( Core.voltInRange(v) ) {
						Core.setVoltDeltaMin(v);
						txtDeltaMin.setBackground(okSetBgnd);
					}
					Core.setReady(true);
				}
			}
		});
		txtDeltaMin.getDocument().addDocumentListener(new DocumentListener() {
			/** Routine to allow valid check during input.
			 * @param e {@link DocumentEvent}
			 */
			private void check(DocumentEvent e) {
				String s = txtDeltaMin.getText();
				if (!s.isEmpty()) {
					int v = Core.getVolt(txtDeltaMin.getText());
					if ( Core.voltInRange(v) ) {
						if (v == Core.getVoltDeltaMin())
							txtDeltaMin.setBackground(okSetBgnd);
						else
							txtDeltaMin.setBackground(okPendingBgnd);
					} else
						txtDeltaMin.setBackground(errBgnd);
				}
			}

			@Override
			public void insertUpdate(DocumentEvent e) {
				check(e);
			}

			@Override
			public void changedUpdate(DocumentEvent e) {
				check(e);
			}

			@Override
			public void removeUpdate(DocumentEvent e) {
				check(e);
			}
		});

		
		lblDeltaMax = new JLabel("Maximum");
		lblDeltaMax.setFont(defaultFont);
		DeltaVoltagePanel.add(lblDeltaMax, "cell 0 2,alignx trailing");
		
		txtDeltaMax = new JTextField();
		txtDeltaMax.setFont(defaultFont);
		txtDeltaMax.setToolTipText("Set maximum voltage (V)");
		txtDeltaMax.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (Core.isReady()) {
					Core.setReady(false);
					int v = Core.getVolt(txtDeltaMax.getText());
					if ( Core.voltInRange(v) ) {
						Core.setVoltDeltaMax(v);
						txtDeltaMax.setBackground(okSetBgnd);
					}
					Core.setReady(true);
				}
			}
		});
		txtDeltaMax.getDocument().addDocumentListener(new DocumentListener() {
			/** Routine to allow valid check during input.
			 * @param e {@link DocumentEvent}
			 */
			private void check(DocumentEvent e) {
				String s = txtDeltaMax.getText();
				if (!s.isEmpty()) {
					int v = Core.getVolt(txtDeltaMax.getText());
					if ( Core.voltInRange(v) ) {
						if (v == Core.getVoltDeltaMax())
							txtDeltaMax.setBackground(okSetBgnd);
						else
							txtDeltaMax.setBackground(okPendingBgnd);
					} else
						txtDeltaMax.setBackground(errBgnd);
				}
			}

			@Override
			public void insertUpdate(DocumentEvent e) {
				check(e);
			}

			@Override
			public void changedUpdate(DocumentEvent e) {
				check(e);
			}

			@Override
			public void removeUpdate(DocumentEvent e) {
				check(e);
			}
		});
		DeltaVoltagePanel.add(txtDeltaMax, "cell 1 2,growx");
		txtDeltaMax.setColumns(10);
		
		VoltagePanel = new JPanel();
		VoltagePanel.setBorder(new TitledBorder(null, "Maximum Voltage", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		ConfigPanel.add(VoltagePanel, "cell 0 1,grow");
		VoltagePanel.setLayout(new MigLayout("", "[][grow]", "[][][]"));
		
		lblVoltChannel = new JLabel("Channel");
		lblVoltChannel.setFont(defaultFont);
		VoltagePanel.add(lblVoltChannel, "cell 0 0,alignx trailing");
		
		comboBoxVolt = new JComboBox();
		comboBoxVolt.setFont(defaultFont);
		VoltagePanel.add(comboBoxVolt, "cell 1 0,growx");
		
		lblVoltMin = new JLabel("Minimum");
		lblVoltMin.setFont(defaultFont);
		VoltagePanel.add(lblVoltMin, "cell 0 1,alignx trailing");
		
		txtVoltMin = new JTextField();
		txtVoltMin.setFont(defaultFont);
		txtVoltMin.setToolTipText("Set minimum voltage (V)");
		txtVoltMin.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (Core.isReady()) {
					Core.setReady(false);
					int v = Core.getVolt(txtVoltMin.getText());
					if ( Core.voltInRange(v) ) {
						Core.setVoltMin(v);
						txtVoltMin.setBackground(okSetBgnd);
					}
					Core.setReady(true);
				}
			}
		});
		txtVoltMin.getDocument().addDocumentListener(new DocumentListener() {
			/** Routine to allow valid check during input.
			 * @param e {@link DocumentEvent}
			 */
			private void check(DocumentEvent e) {
				String s = txtVoltMin.getText();
				if (!s.isEmpty()) {
					int v = Core.getVolt(txtVoltMin.getText());
					if ( Core.voltInRange(v) ) {
						if (v == Core.getVoltMin())
							txtVoltMin.setBackground(okSetBgnd);
						else
							txtVoltMin.setBackground(okPendingBgnd);
					} else
						txtVoltMin.setBackground(errBgnd);
				}
			}

			@Override
			public void insertUpdate(DocumentEvent e) {
				check(e);
			}

			@Override
			public void changedUpdate(DocumentEvent e) {
				check(e);
			}

			@Override
			public void removeUpdate(DocumentEvent e) {
				check(e);
			}
		});
		VoltagePanel.add(txtVoltMin, "cell 1 1,growx");
		txtVoltMin.setColumns(10);
		
		lblVoltMax = new JLabel("Maximum");
		lblVoltMax.setFont(defaultFont);
		VoltagePanel.add(lblVoltMax, "cell 0 2,alignx trailing");
		
		txtVoltMax = new JTextField();
		txtVoltMax.setFont(defaultFont);
		txtVoltMax.setToolTipText("Set maximum voltage (V)");
		txtVoltMax.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (Core.isReady()) {
					Core.setReady(false);
					int v = Core.getVolt(txtVoltMax.getText());
					if ( Core.voltInRange(v) ) {
						Core.setVoltMax(v);
						txtVoltMax.setBackground(okSetBgnd);
					}
					Core.setReady(true);
				}
			}
		});
		txtVoltMax.getDocument().addDocumentListener(new DocumentListener() {
			/** Routine to allow valid check during input.
			 * @param e {@link DocumentEvent}
			 */
			private void check(DocumentEvent e) {
				String s = txtVoltMax.getText();
				if (!s.isEmpty()) {
					int v = Core.getVolt(txtVoltMax.getText());
					if ( Core.voltInRange(v) ) {
						if (v == Core.getVoltMax())
							txtVoltMax.setBackground(okSetBgnd);
						else
							txtVoltMax.setBackground(okPendingBgnd);
					} else
						txtVoltMax.setBackground(errBgnd);
				}
			}

			@Override
			public void insertUpdate(DocumentEvent e) {
				check(e);
			}

			@Override
			public void changedUpdate(DocumentEvent e) {
				check(e);
			}

			@Override
			public void removeUpdate(DocumentEvent e) {
				check(e);
			}
		});
		VoltagePanel.add(txtVoltMax, "cell 1 2,growx");
		txtVoltMax.setColumns(10);
		
		TemperaturePanel = new JPanel();
		TemperaturePanel.setBorder(new TitledBorder(null, "Temperature", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		ConfigPanel.add(TemperaturePanel, "cell 0 2,grow");
		TemperaturePanel.setLayout(new MigLayout("", "[][grow]", "[][][]"));
		
		lblTempChannel = new JLabel("Channel");
		lblTempChannel.setFont(defaultFont);
		TemperaturePanel.add(lblTempChannel, "cell 0 0,alignx trailing");
		
		comboBoxTemp = new JComboBox();
		comboBoxTemp.setFont(defaultFont);
		TemperaturePanel.add(comboBoxTemp, "cell 1 0,growx");
		
		lblTempMin = new JLabel("Minimum");
		lblTempMin.setFont(defaultFont);
		TemperaturePanel.add(lblTempMin, "cell 0 1,alignx trailing");
		
		txtTempMin = new JTextField();
		txtTempMin.setFont(defaultFont);
		txtTempMin.setToolTipText("Set minimum temperature (\u00B0C)");
		txtTempMin.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (Core.isReady()) {
					Core.setReady(false);
					int t = Core.getTemp(txtTempMin.getText());
					if ( Core.tempInRange(t) ) {
						Core.setTempMin(t);
						txtTempMin.setBackground(okSetBgnd);
					}
					Core.setReady(true);
				}
			}
		});
		txtTempMin.getDocument().addDocumentListener(new DocumentListener() {
			/** Routine to allow valid check during input.
			 * @param e {@link DocumentEvent}
			 */
			private void check(DocumentEvent e) {
				String s = txtTempMin.getText();
				if (!s.isEmpty()) {
					int t = Core.getTemp(txtTempMin.getText());
					if ( Core.tempInRange(t) ) {
						if (t == Core.getTempMin())
							txtTempMin.setBackground(okSetBgnd);
						else
							txtTempMin.setBackground(okPendingBgnd);
					} else
						txtTempMin.setBackground(errBgnd);
				}
			}

			@Override
			public void insertUpdate(DocumentEvent e) {
				check(e);
			}

			@Override
			public void changedUpdate(DocumentEvent e) {
				check(e);
			}

			@Override
			public void removeUpdate(DocumentEvent e) {
				check(e);
			}
		});
		TemperaturePanel.add(txtTempMin, "cell 1 1,growx");
		txtTempMin.setColumns(10);
		
		lblTempMax = new JLabel("Maximum");
		lblTempMax.setFont(defaultFont);
		TemperaturePanel.add(lblTempMax, "cell 0 2,alignx trailing");
		
		txtTempMax = new JTextField();
		txtTempMax.setFont(defaultFont);
		txtTempMax.setToolTipText("Set maximum temperature (\u00B0C)");
		txtTempMax.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (Core.isReady()) {
					Core.setReady(false);
					int t = Core.getTemp(txtTempMax.getText());
					if ( Core.tempInRange(t) ) {
						Core.setTempMax(t);
						txtTempMax.setBackground(okSetBgnd);
					}
					Core.setReady(true);
				}
			}
		});
		txtTempMax.getDocument().addDocumentListener(new DocumentListener() {
			/** Routine to allow valid check during input.
			 * @param e {@link DocumentEvent}
			 */
			private void check(DocumentEvent e) {
				String s = txtTempMax.getText();
				if (!s.isEmpty()) {
					int t = Core.getTemp(txtTempMax.getText());
					if ( Core.tempInRange(t) ) {
						if (t == Core.getTempMax())
							txtTempMax.setBackground(okSetBgnd);
						else
							txtTempMax.setBackground(okPendingBgnd);
					} else
						txtTempMax.setBackground(errBgnd);
				}
			}

			@Override
			public void insertUpdate(DocumentEvent e) {
				check(e);
			}

			@Override
			public void changedUpdate(DocumentEvent e) {
				check(e);
			}

			@Override
			public void removeUpdate(DocumentEvent e) {
				check(e);
			}
		});
		TemperaturePanel.add(txtTempMax, "cell 1 2,growx");
		txtTempMax.setColumns(10);
		
		JPanel MonitorPanel = new JPanel();
		MonitorPanel.setBorder(new TitledBorder(null, "Channel Monitor", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		contentPane.add(MonitorPanel, "cell 1 1,grow");
		MonitorPanel.setLayout(new MigLayout("", "[]", "[]"));
		table = new JTable(tableModel);
		table.setFont(defaultFont);
		MonitorPanel.add(new JScrollPane(table), "cell 0 0,grow");
				
		for (int i=0; i<16; i++)
			table.setValueAt(new Integer(i), i, 0);
		
		
		for (int i=0; i<Core.comPortNumber();i++)
			comboBoxComm.addItem(Core.comPortString(i));
		
		for (int i=0; i < 16; i++) {
			comboBoxDelta.addItem(String.valueOf(i));
			comboBoxVolt.addItem(String.valueOf(i));
			comboBoxTemp.addItem(String.valueOf(i));
		}
		comboBoxDelta.addItem("<off>");
		comboBoxVolt.addItem("<off>");
		comboBoxTemp.addItem("<off>");
		
		disableConfig();
		
		timer = new Timer();
		timer.schedule( new commTimer(), 20, 20 );
				
		ready = true;
		
		this.setTitle(Core.getProgVerName());
		
		this.setVisible(true);

	}
	
	void exit() {
		Core.disconnect();
		System.exit(0);
	}
	
	void setTable(int row, String value, String unit) {
		table.setValueAt(value, row, 1);;
		table.setValueAt(unit, row, 2);
	}
	
	void disableConfig() {
		comboBoxDelta.setEnabled(false);
		comboBoxVolt.setEnabled(false);
		comboBoxTemp.setEnabled(false);
		txtDeltaMax.setEnabled(false);
		txtDeltaMin.setEnabled(false);
		txtVoltMax.setEnabled(false);
		txtVoltMin.setEnabled(false);
		txtTempMax.setEnabled(false);
		txtTempMin.setEnabled(false);
		
		btnReadConfig.setEnabled(false);
		btnWriteConfig.setEnabled(false);
	}
	
	void setConfig() {
		int value;
		value = Core.getVoltDeltaChannel();
		if (value > 15)
			value = 16;
		comboBoxDelta.setSelectedIndex(value);
		value = Core.getVoltChannel();
		if (value > 15)
			value = 16;
		comboBoxVolt.setSelectedIndex(value);
		value = Core.getTempChannel();
		if (value > 15)
			value = 16;
		comboBoxTemp.setSelectedIndex(value);
		txtDeltaMax.setText(Core.strVolt(Core.getVoltDeltaMax()));
		txtDeltaMin.setText(Core.strVolt(Core.getVoltDeltaMin()));
		txtVoltMax.setText(Core.strVolt(Core.getVoltMax()));
		txtVoltMin.setText(Core.strVolt(Core.getVoltMin()));
		txtTempMax.setText(Core.strVolt(Core.getTempMax()));
		txtTempMin.setText(Core.strVolt(Core.getTempMin()));
		
		comboBoxDelta.setEnabled(true);
		comboBoxVolt.setEnabled(true);
		comboBoxTemp.setEnabled(true);
		txtDeltaMax.setEnabled(true);
		txtDeltaMin.setEnabled(true);
		txtVoltMax.setEnabled(true);
		txtVoltMin.setEnabled(true);
		txtTempMax.setEnabled(true);
		txtTempMin.setEnabled(true);
		
		btnReadConfig.setEnabled(true);
		btnWriteConfig.setEnabled(true);
	}
	
	private void readComboBoxes() {		
		Core.setReady(false);
		int idx = comboBoxDelta.getSelectedIndex();
		if (idx > 15)
			idx = 0xff;
		Core.setVoltDeltaChannel(idx);
		idx = comboBoxVolt.getSelectedIndex();
		if (idx > 15)
			idx = 0xff;
		Core.setVoltChannel(idx);
		idx = comboBoxTemp.getSelectedIndex();
		if (idx > 15)
			idx = 0xff;
		Core.setTempChannel(idx);


		Core.setReady(true);
	}
	
	private void readTextFields() {
		Core.setReady(false);
		int v = Core.getVolt(txtDeltaMin.getText());
		if ( Core.voltInRange(v) ) {
			Core.setVoltDeltaMin(v);
			txtDeltaMin.setBackground(okSetBgnd);
		}
		v = Core.getVolt(txtDeltaMax.getText());
		if ( Core.voltInRange(v) ) {
			Core.setVoltDeltaMax(v);
			txtDeltaMax.setBackground(okSetBgnd);
		} 
		v = Core.getVolt(txtVoltMin.getText());
		if ( Core.voltInRange(v) ) {
			Core.setVoltMin(v);
			txtVoltMin.setBackground(okSetBgnd);
		}
		v = Core.getVolt(txtVoltMax.getText());
		if ( Core.voltInRange(v) ) {
			Core.setVoltMax(v);
			txtVoltMax.setBackground(okSetBgnd);
		} 
		int t = Core.getTemp(txtTempMin.getText());
		if ( Core.tempInRange(t) ) {
			Core.setTempMin(t);
			txtTempMin.setBackground(okSetBgnd);
		}
		t = Core.getTemp(txtTempMax.getText());
		if ( Core.tempInRange(t) ) {
			Core.setTempMax(t);
			txtTempMax.setBackground(okSetBgnd);
		} 
		Core.setReady(true);
	}
	
	
	
	class commTimer extends TimerTask {
		@Override
		public void run() {			
			
			if (Core.isConnected()) {
				Core.scan();
			}
		}
	}

}

